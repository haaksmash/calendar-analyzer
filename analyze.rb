# frozen_string_literal: true

Bundler.require(:default, :development)

FORMAT = ENV['FORMAT'] || 'CLI'
DEBUG_LEVEL = ENV['DEBUG'].to_i || 0
LOOKBACK_DAYS = ENV['LOOKBACK'].to_i || 40

class Event
  attr_reader :start, :end, :summary

  def initialize(start:, end_:, summary:)
    @start = start
    @end = end_
    @summary = summary
  end

  def to_s
    time_string =
      if DEBUG_LEVEL > 2
        "#{@start.strftime('%D::%H:%M')}--#{@end.strftime('%D::%H:%M')}"
      else
        "#{@start.strftime('%H:%M')}--#{@end.strftime('%H:%M')}"
      end

    "#{time_string} : #{@summary}"
  end
end

class Fragmentation
  # the relative 'badness' of these transitions:
  # * a no-meetings day should have 0 fragmentation.
  # * back-to-back meetings are preferable to spaced-out meetings
  # * the length of a meeting doesn't matter
  # * the length of a free slot doesn't matter
  COST_OF_FREE_TO_UNFREE = 1.0
  COST_OF_UNFREE_TO_UNFREE = 0.3
  COST_OF_UNFREE_TO_FREE = 0
  COST_OF_FREE_TO_FREE = 0

  def self.for_day(event_list)
    event_list.reduce({ score: 0, last_event_type: 'FREE' }) do |score_object, event|
      if event.summary != 'FREE'
        this_event_type = 'UNFREE'
        if score_object[:last_event_type] == 'FREE'
          new_score = COST_OF_FREE_TO_UNFREE
        else
          new_score = COST_OF_UNFREE_TO_UNFREE
        end
      else
        this_event_type = 'FREE'
        if score_object[:last_event_type] == 'FREE'
          new_score = COST_OF_FREE_TO_FREE
        else
          new_score = COST_OF_UNFREE_TO_FREE
        end
      end

      if DEBUG_LEVEL > 0
        puts " #{event.to_s}: #{new_score}"
      end
      {
        score: score_object[:score] + new_score,
        last_event_type: this_event_type,
      }
    end[:score]
  end
end

def print_calendar_list(list, leftpad)
  list.each { |e| puts "#{leftpad}#{e.to_s}" }
end

class CliEmitter
  def initialize(date_range, stats_by_day)
    @date_range = date_range
    @stats_by_day = stats_by_day
  end

  def emit!
    @date_range.each do |day|
      next if day.saturday? || day.sunday?
      stats = @stats_by_day[day]
      if !stats.nil?
        puts "#{day}"
        puts "  #{stats[:event_lengths].size} events (#{stats[:average_event_length].round(0)} minutes avg)"
        puts "  #{(stats[:fraction_non_free] * 100).round(0)}% of day lost to events"
        puts "  Fragmentation score: #{stats[:fragmentation]}"
      end
    end
  end
end

class CsvEmitter
  COLUMNS = ['day', 'user', 'fragmentation score', 'percent in meetings']
  def initialize(calendar_id, date_range, stats_by_day)
    @calendar_id = calendar_id
    @date_range = date_range
    @stats_by_day = stats_by_day
  end

  def emit!
    rows = @date_range.map do |day|
      next if day.saturday? || day.sunday?
      stats = @stats_by_day[day]
      if stats.nil?
        { 'day' => day.to_s, 'user' => @calendar_id, 'fragmentation score' => 0.0, 'percent in meetings' => 0.0 }
      else
        {
          'day' => day.to_s,
          'user' => @calendar_id,
          'fragmentation score' => stats[:fragmentation],
          'percent in meetings' => (stats[:fraction_non_free] * 100).round(0),
        }
      end
    end.compact

    puts COLUMNS.join(',')
    rows.each do |row|
      puts COLUMNS.map { |c| row[c] }.join(',')
    end
  end
end

class EventCollector
  OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'.freeze
  APPLICATION_NAME = 'Calendar Analysis'.freeze
  CREDENTIALS_PATH = 'credentials.json'.freeze
  # The file token.yaml stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  TOKEN_PATH = 'token.yaml'.freeze
  SCOPE = Google::Apis::CalendarV3::AUTH_CALENDAR_READONLY

  EXCLUDED_EVENT_NAMES = [
    'in denver',
    'dns',
    'busy',
    'transit to work',
    'transit home',
    'hold for office hours',
    'vet',
    'aerials',
    'board games!',
    'out of office',
    'haircut',
    'no meetings',
    'no interviews',
    'ic time',
    'wfh',
    'commuting',
    'vm',
    'on call',
    'flight',
    'head to airport from office?',
    'pairing',
    'ic time',
  ]

  def initialize(date_range)
    @date_range = date_range
    # Initialize the API
    @service = Google::Apis::CalendarV3::CalendarService.new
    @service.client_options.application_name = APPLICATION_NAME
    @service.authorization = authorize
  end

  ##
  # Ensure valid credentials, either by restoring from the saved credentials
  # files or intitiating an OAuth2 authorization. If authorization is required,
  # the user's default browser will be launched to approve the request.
  #
  # @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
  def authorize
    client_id = Google::Auth::ClientId.from_file(CREDENTIALS_PATH)
    token_store = Google::Auth::Stores::FileTokenStore.new(file: TOKEN_PATH)
    authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store)
    user_id = 'default'
    credentials = authorizer.get_credentials(user_id)
    if credentials.nil?
      url = authorizer.get_authorization_url(base_url: OOB_URI)
      puts 'Open the following URL in the browser and enter the ' \
           "resulting code after authorization:\n" + url
      code = gets
      credentials = authorizer.get_and_store_credentials_from_code(
        user_id: user_id, code: code, base_url: OOB_URI
      )
    end
    credentials
  end

  def list_events(calendar_id)
    @service.list_events(
      calendar_id,
      single_events: true,
      order_by: 'startTime',
      time_min: @date_range.first.to_datetime.rfc3339,
      time_max: (@date_range.last.to_datetime).rfc3339,
    )
      .items
      .reject do |event|
        # only use Yes'd events
        me = event.attendees&.find { |a| a.email == ENV['EMAIL'] }
        !me.nil? && me.response_status != 'accepted'
      end
      .reject { |event| event.summary&.downcase&.start_with?(*EXCLUDED_EVENT_NAMES) }
      .reject { |event| event.start.date_time.nil? } # removes the all-day events, which are not interesting
      .map { |event| Event.new(start: event.start.date_time, end_: event.end.date_time, summary: event.summary) }
  end
end

class Statistics
  def self.for(calendar_id, date_range)
    new.calculate_stats!(calendar_id, date_range)
  end

  def calculate_stats!(calendar_id, date_range)
    calendar_events = EventCollector.new(date_range).list_events(calendar_id)
    if calendar_events.empty?
      return {}
    end

    events_by_day = calendar_events.group_by { |event| event.start.to_date }
    events_by_day.map { |day, events| [day, analyze(day, events)] }.to_h
  end

  def analyze(day, events)
    if events.size == 0
      return
    end
    # remember that timezeones are a thing; datetimes want to be in UTC, so we
    # have to correct this to make future operations a little more intuitive.
    day_start = day.to_datetime.new_offset('-07:00')
    filled_calendar = construct_calendar(events, day_start)

    non_free_blocks = filled_calendar.reject { |e| e.summary == 'FREE' }
    # skip days that are totally free.
    if non_free_blocks.size == 0
      return
    end

    event_lengths = non_free_blocks
      .map { |e| ((e.end - e.start) * 24 * 60).to_f.round(2) }

    free_lengths = filled_calendar.select { |e| e.summary == 'FREE' }
      .map { |e| ((e.end - e.start) * 24 * 60).to_f.round(2) }

    fragmentation_for_day = Fragmentation.for_day(filled_calendar)

    {
      event_lengths: event_lengths,
      free_lengths: free_lengths,
      average_event_length: (event_lengths.sum / event_lengths.size), # minutes
      fraction_non_free: event_lengths.sum / (free_lengths.sum + event_lengths.sum),
      fragmentation: fragmentation_for_day,
    }
  end

  def construct_calendar(events, day_start)
    filled_calendar = [Event.new(start: day_start + 15/24.0, end_: day_start + 26/24.0, summary: 'FREE')]
    puts day_start.to_date if DEBUG_LEVEL > 0
    print_calendar_list(filled_calendar, '') if DEBUG_LEVEL > 0
    filled_calendar = events
      .sort_by { |e| e.start }
      .reduce(filled_calendar) do |calendar_list, event|
        puts "inserting #{event.to_s}" if DEBUG_LEVEL > 0
        # find the the first free block that fully encloses this new event
        first_free = calendar_list.find do |e|
          puts " considering #{e.to_s}" if DEBUG_LEVEL > 1
          if DEBUG_LEVEL > 2
            if e.summary == 'FREE'
              puts '  summary is ok'
            else
              puts '  summary is bad'
            end
            if e.start <= event.start
              puts "  start is ok"
            else
              puts "  start is bad"
            end
            if e.end >= event.end
              puts "  end is ok"
            else
              puts "  end is bad"
            end
          end

          e.start <= event.start && e.end >= event.end && e.summary == 'FREE'
        end
        if first_free.nil?
          if DEBUG_LEVEL > 2
            puts " no free:"
            print_calendar_list(calendar_list, '  ')
          end
          next calendar_list
        end
        # keep our place in the calendar list
        calendar_pivot = calendar_list.find_index(first_free)
        calendar_list.delete_at(calendar_pivot)
        # split the free block
        new_leading_free =
          if event.start != first_free.start
            Event.new(start: first_free.start, end_: event.start, summary: 'FREE')
          else
            puts "matching start dates" if DEBUG_LEVEL > 3
            nil
          end

        new_following_free =
          if event.end != first_free.end
            Event.new(start: event.end, end_: first_free.end, summary: 'FREE')
          else
            puts "matching end dates: #{event.end} | #{first_free.end}" if DEBUG_LEVEL > 3
            nil
          end

        replacement_events = [new_leading_free, event, new_following_free].compact
        replacement_events.each { |e| calendar_list.insert(calendar_pivot, e) }
        calendar_list = calendar_list.sort_by { |e| e.start }
        calendar_list
      end
  end
end

def main(output_format, today = Date.today)
  date_range = (today - LOOKBACK_DAYS)...today

  calendar_id = ENV['EMAIL']
  stats_by_day = Statistics.for(calendar_id, date_range)

  if stats_by_day.empty?
    puts 'No events found'
    return
  end

  emitter =
    if output_format == 'CLI'
      CliEmitter.new(date_range, stats_by_day)
    elsif output_format == 'CSV'
      CsvEmitter.new(calendar_id, date_range, stats_by_day)
    end

  emitter.emit!
end

main(FORMAT)
