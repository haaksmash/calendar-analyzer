# Calendar Analyzer

Little tool to give you a sense of how fractured your day is.

It should be as simple as invoking the obvious script with a less-obvious
environment variable:

```sh
$ EMAIL="me@domain.com" bundle exec ruby analyze.rb
```
It will attempt to OAuth in to your google account; follow the instructions to
allow access.

Provide a `DEBUG` environment variable set to a number to see various levels of
additional processing information:

```sh
$ DEBUG=2 EMAIL="me@domain.com" bundle exec ruby analyze.rb
```

The `EMAIL` environment variable can be set to any calendar you have access to;
your own, your coworkers', etc.

The `FORMAT` environment variable can be set to `CSV` to change the output to a
more machine-friendly format.
