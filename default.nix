with (import <nixpkgs-18.09-darwin> {});

stdenv.mkDerivation {
  name = "calendar-analysis";

  buildInputs = [
    ruby_2_5
  ];
}
